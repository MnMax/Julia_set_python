import numpy as np
from pylab import imshow, show
import matplotlib.pyplot as plt
from PIL import Image

xa = -2.
xb = 2.
ya = -1.5
yb = 1.5

width = 512
height = 512
max_iters = 200

c=-0.72+0.11*1j


z=c

image = np.zeros((height, width), dtype=np.uint8)

pixelX = (xb- xa) / width
pixelY = (yb- ya) / height

for y in range (0,height-1):
    zy= ya + y * (xb-ya)/height
    for x in range (0,width-1):
        zx=xa+x*(xb-xa)/width
        z=zx+zy*1j
        for k in range (0,max_iters-1):
            if abs(z)>=2:
                break
            z=z*z+c
        image[y,x]=k


imshow(image)
show()
plt.imshow(image)
plt.savefig("fractale.png")



im2=Image.open("fractale.png")
NbC,NbL=im2.size

for j in range (NbC):
    for i in range (NbL):
        p=im2.getpixel((j,i))
        im2.putpixel((j,i),(p[0],p[1],p[2]))

im2.save("image2.png")
